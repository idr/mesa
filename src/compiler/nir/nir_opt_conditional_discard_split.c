/*
 * Copyright © 2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "nir.h"
#include "nir_builder.h"

/**
 * \file nir_opt_conditional_discard_split.c
 * Split some conditional discards into two conditional discards
 *
 * If the condition is something like (A || B), the logical operator can be
 * removed, and the original conditional discard is replaced with two new
 * conditional discards.  This sequence
 *
 *    vec1 32 ssa_7 = flt32! ssa_6.z, ssa_1
 *    vec1 32 ssa_8 = flt32! ssa_6.w, ssa_1
 *    vec1 32 ssa_9 = ior! ssa_8, ssa_7
 *    intrinsic discard_if (ssa_9) ()
 *
 * becomes
 *
 *    vec1 32 ssa_7 = flt32! ssa_6.z, ssa_1
 *    vec1 32 ssa_8 = flt32! ssa_6.w, ssa_1
 *    intrinsic discard_if (ssa_7) ()
 *    intrinsic discard_if (ssa_8) ()
 */

static bool
nir_opt_conditional_discard_split_impl(nir_shader *shader,
                                       nir_function_impl *impl)
{
   bool progress = false;

   nir_foreach_block(block, impl) {
      nir_foreach_instr_safe(instr, block) {
         if (instr->type != nir_instr_type_intrinsic)
            continue;

         nir_intrinsic_instr *intrin = nir_instr_as_intrinsic(instr);
         if (intrin->intrinsic != nir_intrinsic_demote_if &&
             intrin->intrinsic != nir_intrinsic_discard_if)
            continue;

         nir_alu_instr *alu = nir_src_as_alu_instr(intrin->src[0]);
         if (alu == NULL || alu->op != nir_op_ior)
            continue;

         nir_intrinsic_instr *discard_if;

         discard_if = nir_intrinsic_instr_create(shader, intrin->intrinsic);
         discard_if->src[0] = alu->src[0].src;
         nir_instr_insert_before(&intrin->instr, &discard_if->instr);

         discard_if = nir_intrinsic_instr_create(shader, intrin->intrinsic);
         discard_if->src[0] = alu->src[1].src;
         nir_instr_insert_before(&intrin->instr, &discard_if->instr);

         nir_instr_remove(&intrin->instr);
         progress = true;
      }
   }

   if (progress) {
      nir_metadata_preserve(impl, nir_metadata_block_index |
                                  nir_metadata_dominance);
   }

   return progress;
}

bool
nir_opt_conditional_discard_split(nir_shader *shader)
{
   bool progress = false;

   nir_foreach_function(function, shader) {
      if (function->impl == NULL)
         continue;

      progress = nir_opt_conditional_discard_split_impl(shader, function->impl)
         || progress;
   }

   return progress;
}
